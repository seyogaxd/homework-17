const tabTitles = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');
const tabsArray = Array.from(tabTitles);
const contentArray = Array.from(tabContents);
tabsArray.forEach(function(tab) {
    tab.onclick = function() {
        var clickedIndex = tabsArray.indexOf(tab);
        changeActive(clickedIndex);
    };
});

function changeActive(tab) {
    tabsArray.forEach(function(tabElement) {
        if(tabElement.classList.contains('active')) {
            tabElement.classList.remove('active');
        }
    });
    tabsArray[tab].classList.add('active');
    contentArray.forEach(function(contentElement) {
        contentElement.style.display = "none";
    });
    contentArray[tab].style.display = "flex";
};

changeActive(0);